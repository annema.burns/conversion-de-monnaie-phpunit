<?php
declare(strict_types=1);

require_once(__DIR__ . "/../../src/entites/Monnaie.class.php");

use PHPUnit\Framework\TestCase;

final class MonnaieTest extends TestCase
{

    protected $monnaie;
    protected $record;

    protected function setUp(): void
    {
        $this->monnaie = new Monnaie();
        $this->record['NOM'] = 'Euro';
        $this->record['VALEUR'] = 1.5;
    }

    public function testCreationTauxMonnaie(): void
    {

        $this->assertInstanceOf(
            Monnaie::class,
            $this->monnaie
        );
    }

    public function testLoadFromRecord(): void
    {

        $this->monnaie->loadfromrecord($this->record);

        $this->assertEquals($this->record['NOM'], $this->monnaie->getNom());
        $this->assertEqualsWithDelta($this->record['VALEUR'], $this->monnaie->getTaux(), 0.001);
    }

    public function testLoadFromRecordTauxNegatif(): void
    {

        $this->expectException(IllegalArgumentException::class);

        $this->record['VALEUR'] = -$this->record['VALEUR'];
        $this->monnaie->loadfromrecord($this->record);

    }

    public function testSetNom(): void
    {

        $this->monnaie->setNom($this->record['NOM']);

        $this->assertEquals($this->record['NOM'], $this->monnaie->getNom());
    }

    public function testSetTaux(): void
    {

        $this->monnaie->setTaux($this->record['VALEUR']);

        $this->assertEqualsWithDelta($this->record['VALEUR'], $this->monnaie->getTaux(), 0.001);
    }

    public function testSetTauxNegatif(): void
    {

        $this->expectException(IllegalArgumentException::class);

        $this->monnaie->setTaux(-$this->record['VALEUR']);

    }

}