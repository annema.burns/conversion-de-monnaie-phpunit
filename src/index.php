<?php
	// -- Contrôleur frontal --
	require_once(__DIR__ . '/controleurs/Routeur.class.php');

	if (ISSET($_REQUEST["action"]))
		{
			//$vue = ActionBuilder::getAction($_REQUEST["action"])->execute();
			/*
			Ou :*/
			$actionDemandee = $_REQUEST["action"];
			$controleur = Routeur::getAction($actionDemandee);
			$vue = $controleur->execute();
			/**/
		}
	else	
		{
			$action = Routeur::getAction("convertir");
			$vue = $action->execute();
		}

	echo $vue->genererContenu();
?>
