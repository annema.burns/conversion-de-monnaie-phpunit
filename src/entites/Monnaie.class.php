<?php

require_once(__DIR__ . "/../exceptions/IllegalArgumentException.class.php");

class Monnaie {
	private $nom = "";
	private $taux = 0.0;

	public function __construct()	//Constructeur
	{

	}	
	
	public function getNom()
	{
			return $this->nom;
	}
	
	public function setNom($value)
	{
			$this->nom = $value;
	}

	public function getTaux()
	{
			return $this->taux;
	}
	
	public function setTaux($value)
	{
		if($value < 0) {
			throw new IllegalArgumentException("Le taux de conversion de la monnaie ne peut pas être négatif");
		}
		$this->taux = $value;
	}
	
	public function __toString()
	{
		return "Monnaie[".$this->nom.", ".$this->taux."]";
	}
	public function affiche()
	{
		echo $this->__toString();
	}
	public function loadFromRecord($ligne)
	{
		$this->setNom($ligne["NOM"]);
		$this->setTaux($ligne["VALEUR"]);
	}	
}
?>