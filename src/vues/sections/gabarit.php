<!DOCTYPE html>
<html>
	<head>
	  	<meta charset="utf-8" />
	  	<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="./css/style.css" type="text/css" />
		<title><?php echo $titre; ?></title>
	</head>

	<body>
	<div>
	<?php
		include("banner.php");
	?>
		<main id="content">
			<?php echo $contenu; ?>
		</main>
	<?php
		include("footer.php");
	?>
	</div>
	</body>
</html>