
<h2>Convertisseur de monnaies</h2>
<form>
    <input type="hidden" name="action" value="convertir" />
    Montant : <input type="text" name="montant" />
    <select name="monnaie">
        <option value="">--Choisir--</option>
        <?php

			while (count($data['monnaies']) > 0)
			{

				$uneMonnaie = array_pop($data['monnaies']);
				if ($uneMonnaie != null)
				{
					echo "<option value=" . $uneMonnaie . ">" . $uneMonnaie . "</option>";
				}
			}

		?>
    </select>
    <input type="submit" value="OK" />
</form>
<?php
	if(ISSET($data['resultat'])) {
		echo "Montant équivalent : <span class='resultat'>" . $data['resultat'] . " \$CAN</span>";
	}
?>


