<?php
require_once(__DIR__ . '/../db/classes/Database.class.php'); 
require_once(__DIR__ . '/../entites/Monnaie.class.php'); 

class MonnaieDAO
{	

	public static function getListeMonnaies()
	{
		try {
		    $liste = array();

			$requete = 'SELECT nom FROM monnaie';
			$cnx = Database::getInstance();
			
			$res = $cnx->query($requete);
		    foreach($res as $row) {
				array_push($liste, $row['nom']);
		    }
			$res->closeCursor();
		    $cnx = null;
			return $liste;
		} catch (PDOException $e) {
		    print "Error!: " . $e->getMessage() . "<br/>";
		    return $liste;
		}	
	}	

	public static function getMonnaie($id)
	{
		$db = Database::getInstance();

		$pstmt = $db->prepare("SELECT * FROM monnaie WHERE nom = :x");//requ�te param�tr�e par un param�tre x.
		$pstmt->execute(array(':x' => $id));
		
		$result = $pstmt->fetch(PDO::FETCH_OBJ);
		$p = new Monnaie();

		if ($result)
		{
			$p->setNom($result->NOM);
			$p->setTaux($result->VALEUR);
			$pstmt->closeCursor();
			return $p;
		}
		$pstmt->closeCursor();
		return null;
	}

	
}
?>