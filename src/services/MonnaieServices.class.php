<?php
require_once(__DIR__ . '/../daos/MonnaieDAO.class.php');

class MonnaieServices {

	public static function getNomsMonnaies()
	{
		$dao = new MonnaieDAO();
		return $dao->getListeMonnaies();
	}

	public static function doConversion($monnaie, $montant){
		$dao = new MonnaieDAO();
		$montantConverti = 0;

		$monnaie = $dao->getMonnaie($monnaie);

		// le taux de cette monnaie n'est pas connu
		if (is_null($monnaie)) {
			return $montantConverti;
		}

		$taux = $monnaie->getTaux();
		$montantConverti = $montant * $taux;
		return $montantConverti;
	}
}
?>