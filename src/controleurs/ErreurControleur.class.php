<?php
require_once(__DIR__ . '/Action.interface.php');
require_once(__DIR__ . '/../vues/Page.class.php');

class ErreurControleur implements Action {
	public function execute(){
		return new Page("erreur", "Mon site - Page inexistante", null, null);
	}
}
?>