<?php
require_once(__DIR__ . '/ConvertirControleur.class.php');
require_once(__DIR__ . '/ErreurControleur.class.php');

class Routeur{
	public static function getAction($nomAction){

		$classe = ucfirst($nomAction) . 'Controleur';
		if (class_exists($classe)) {
			return new $classe();
		} else {
			return new ErreurControleur();
		}

	}
}
?>
