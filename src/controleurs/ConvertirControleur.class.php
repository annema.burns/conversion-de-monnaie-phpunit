<?php
require_once(__DIR__ . '/Action.interface.php');
require_once(__DIR__ . '/../vues/Page.class.php');
require_once(__DIR__ . '/../entites/Monnaie.class.php');
require_once(__DIR__ . '/../services/MonnaieServices.class.php');

class ConvertirControleur implements Action {
	public function execute(){

		$data['monnaies'] = MonnaieServices::getNomsMonnaies();

		if (ISSET($_REQUEST["montant"]) && ISSET($_REQUEST["monnaie"])) {
			$montant = $_REQUEST["montant"];
			$monnaie = $_REQUEST["monnaie"];
			$data['resultat'] = MonnaieServices::doConversion($monnaie, $montant);

		}

		return new Page("convertir", "Convertisseur de monnaies", $data, null);
	}
}
?>