-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Lun 15 Octobre 2018 à 14:59
-- Version du serveur :  5.6.20-log
-- Version de PHP :  5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `conversion`
--

-- --------------------------------------------------------

--
-- Structure de la table `monnaie`
--

CREATE TABLE IF NOT EXISTS `monnaie` (
  `NOM` varchar(48) NOT NULL,
  `VALEUR` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `monnaie`
--

INSERT INTO `monnaie` (`NOM`, `VALEUR`) VALUES
('Euro', 1.5),
('$US', 1.25),
('GBP', 1.7),
('CHF', 1.3),
('CNY', 0.2),
('TRY', 0.33);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `monnaie`
--
ALTER TABLE `monnaie`
 ADD PRIMARY KEY (`NOM`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
