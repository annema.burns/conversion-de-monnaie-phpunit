# Conversion de monnaie PHPUnit

Cet exemple reprend la solution proposée par A. Toudef pour le  laboratoire du chapitre 9 du cours hypermédia 2, session automne 2019.

Des tests unitaires sont ajoutés à la solution afin d'en démontrer l'utilisation dans le cadre du cours de développement de projets informatiques (420-AW6), session automne 2019.

L'[exemple original](https://gitlab.com/annema.burns/conversion-de-monnaie-avec-tests-unitaires) en Java EE a été réécrit en PHP.

Les tests peuvent être exécutés en utilisant la commande suivante à la racine du projet :

`phpunit --configuration tests/phpunit.xml`